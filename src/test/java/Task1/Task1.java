package Task1;

import static io.restassured.RestAssured.given;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.asserts.SoftAssert;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

public class Task1 {
	 

	
	public Response  PostRequest(String baseUrl, String endPoint, String jsonFilePath) {
		
		
		
//	given().relaxedHTTPSValidation().when().post("https://my_server.com");

		RestAssured.baseURI= baseUrl;

		Response Res=given().header("Content-Type","application/json")
					 .body(new File(jsonFilePath)).when().post(endPoint)
					 .then().extract().response();
		
		return Res;
		
	}
 
	public String GetPropVal(String propertyRetrived, String propFilePath) throws IOException {
		String val = null; 

		try (InputStream input = new FileInputStream(propFilePath)) {

			Properties prop = new Properties();
			prop.load(input);
			val = prop.getProperty(propertyRetrived);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return val;  
	}
	
	public JSONObject  convertFileToObject(String jsonFilePath ) throws ParseException {
		
			JSONParser parser = new JSONParser();
		
		try {
			Object object = parser.parse(new FileReader(jsonFilePath));
            
            //convert Object to JSONObject
            JSONObject jsonObject = (JSONObject)object;
	        
            return jsonObject;
            
	    } catch (IOException  e) {
	        e.printStackTrace();
	        
	        return null;
	    }
	
	}
	public boolean nullcheck(String val) {
		
		boolean returnValue;
		if(val.trim().isEmpty() || val == null ) {
			
			returnValue = false;
			
		}
		else {
			returnValue = true;
		}
		
		return returnValue;
	}


public void softAssert(boolean expected, boolean actual, String  message) {
	

}


}
