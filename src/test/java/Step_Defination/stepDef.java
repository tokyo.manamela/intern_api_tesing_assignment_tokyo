package Step_Defination;

import org.json.simple.JSONObject;
import org.junit.Assert;
import Task1.Task1;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;

public class stepDef extends Task1  {

	Task1 obj = new Task1();

	String jsonFilePath = "src/test/resources/Payloads/addNewUser.Json";
	String propertyFilepath = "src/test/resources/Resource/config.properties";
	Response res;
	Scenario scenario;
	int statusCode = 0;
	JSONObject jsonObj = null;


	@Before
	public void beforeStep(Scenario scenario) {
		this.scenario = scenario;
	}

	@Given("^the user details to be sent are not null$")
	public void the_user_details_to_be_sent_are_not_null() throws Throwable {

		
		 jsonObj = convertFileToObject(jsonFilePath);
		 
		 scenario.write("Find payload below \n"+jsonObj.toString());
		 
		 Assert.assertNotNull("The json object/ file is empty", jsonObj);
		 
		 
	}

	
	@When("^a request is send the the /users endpoint$")
	public void a_request_is_send_the_the_users_endpoint() throws Throwable {


		//sending request
		res= PostRequest(GetPropVal("BaseURL", propertyFilepath),GetPropVal("endPoint", propertyFilepath),jsonFilePath);
	
		 scenario.write("Find response below \n"+res.asString());
		 Assert.assertNotNull("The response is Empty/ null", res);
	}

	@When("^The status code return is OK$")
	public void the_status_code_return_is_OK() throws Throwable {
	  
		statusCode= res.getStatusCode();
		
		Assert.assertEquals(201, statusCode);
		
		
	}
	@Then("^Validate if the response is corresponds with Response$")
	public void validate_id_the_resonse_is_correct() throws Throwable {
		
	} 

}




